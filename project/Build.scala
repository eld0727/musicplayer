import sbt._
import Keys._

object ApplicationBuild extends Build {
    val appName         = "player"
    val appVersion      = "0.1.0-SNAPSHOT"

    val appDependencies = Seq(
    )

    lazy val proj = Project(
        id = appName,
        base = file("."),
        settings = Defaults.defaultSettings
          ) 

}

resolvers ++= Seq(
  "typesafe releases" at "http://repository.tpu.ru/typesafe-releases/",
  "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repo",
  Resolver.url("sbt-plugin-releases", new URL("http://repository.tpu.ru/sbt-plugin-releases"))(Resolver.ivyStylePatterns)
)

addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "2.1.0")
